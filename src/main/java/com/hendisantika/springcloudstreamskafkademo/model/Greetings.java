package com.hendisantika.springcloudstreamskafkademo.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-cloud-streams-kafka-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/03/18
 * Time: 21.56
 * To change this template use File | Settings | File Templates.
 */
@Getter
@Setter
@ToString
@Builder
public class Greetings {
    private long timestamp;
    private String message;
}