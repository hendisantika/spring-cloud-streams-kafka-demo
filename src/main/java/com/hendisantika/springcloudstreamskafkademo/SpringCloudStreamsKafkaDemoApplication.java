package com.hendisantika.springcloudstreamskafkademo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudStreamsKafkaDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudStreamsKafkaDemoApplication.class, args);
    }
}
