package com.hendisantika.springcloudstreamskafkademo.config;

import com.hendisantika.springcloudstreamskafkademo.stream.GreetingsStreams;
import org.springframework.cloud.stream.annotation.EnableBinding;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-cloud-streams-kafka-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/03/18
 * Time: 21.57
 * To change this template use File | Settings | File Templates.
 */
@EnableBinding(GreetingsStreams.class)
public class StreamsConfig {
}
